<?php

namespace HydroDemoApp\Service;

/**
 * Basic demonstration class for a factory controller
 *
 * This demonstration class could be used to be executed, everytime no other controller is found, so it generates out of the tried controller class name the template file name
 *
 * @category
 * @package    View\Flood\Installation
 * @author     Original Author mb.project@bemit.eu
 * @link
 * @copyright  2016 - 2017
 * @since      Version
 * @version    Release: @package_version@
 */
class Factory {

    /**
     * @param $view \Flood\View\View
     */
    public function __construct($view) {
        //parent::__construct($view);
        $this->template_file = $view->class_name . '.twig';
    }

    public function call() {
        $this->display();
    }
}