<?php

namespace HydroDemoApp\Controller;

use \Hydro\Config;
use \Hydro\Template\Template;

/**
 * Short description for class
 *
 * Long description for class
 *
 * @category
 * @package
 * @author     Original Author mb.project@bemit.eu
 * @link
 * @copyright  2017
 * @since      Version
 * @version    Release: @package_version@
 */
class Flow {

    protected $template;

    public function __construct() {
        $this->template = new Template();
    }

    public function render() {
        $this->template->assign('demo', 'Lorem ipsulum');
        $this->template->render(Config::serverPath(true) . 'vendor/flood/hydro-demo-app/View/Flow.twig');
        var_dump('Flow->render()');
    }
}