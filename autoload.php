<?php
spl_autoload_register(function ($class) {
    $prefix = 'HydroDemoApp\\';
    $base_dir = __DIR__.'/';
    $prefix_length = strlen($prefix);
    if (strncmp($prefix, $class, $prefix_length) !== 0) {
        //When not the Flood namespace, move to next autoloader
        return;
    }
    $relative_class_name = substr($class, $prefix_length);
    $class_path = $base_dir . str_replace('\\', '/', $relative_class_name) . '.php';
    if (file_exists($class_path)) {
        require $class_path;
    }
});